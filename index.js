function randNum(){
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            let numero = Math.floor(Math.random() * 10) + 1;
            if (numero < 5 ){
                resolve (numero);
            }
            else {
                reject("o numero é maior que 5");
            }
        },3000);
    });
}

//testando função com then e catch
randNum()
    .then((numero)=> {
        console.log("Um número foi gerado ", numero);
    })
    .catch((erro)=>{
        console.error(erro);
    });
